package ch.barbulescu.coroutines

import io.github.oshai.kotlinlogging.KotlinLogging
import kotlinx.coroutines.*

private val logger = KotlinLogging.logger {}

fun main() = runBlocking("root") {
    work()
    logger.info { "finish" }
}

suspend fun work() {
    supervisorScope {
        launch {
            delay(1000)
            logger.info { "g1 end" }
        }
        launch("g2") {
            launch("inner1") {
                delay(1000)
                logger.info { "inner 1" }
            }
            delay(2000)
            launch("inner2") {
                delay(1000)
                logger.info { "inner 2" }
            }
            logger.info { "g2 end" }
        }
        logger.info { "work started" }
    }
}

fun runBlocking(name: String, block: suspend CoroutineScope.() -> Unit) =
    runBlocking(Dispatchers.IO + CoroutineName(name), block)

private fun CoroutineScope.name(value: String) =
    CoroutineName(coroutineContext[CoroutineName]?.name + "/$value")

private fun CoroutineScope.launch(name: String, block: suspend CoroutineScope.() -> Unit): Job =
    this.launch(context = name(name), block = block)
