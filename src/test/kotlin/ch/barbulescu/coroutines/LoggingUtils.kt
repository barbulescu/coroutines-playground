package ch.barbulescu.coroutines

import java.time.LocalTime

fun log(text: String) {
    println("[${Thread.currentThread().name}] - ${LocalTime.now()} - $text")
}