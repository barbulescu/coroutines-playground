package ch.barbulescu.coroutines

import io.kotest.common.runBlocking
import io.kotest.core.spec.style.StringSpec
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class LaunchTest : StringSpec({
    "start in scope" {
        runBlocking {
            coroutineScope {
                launch {
                    delay(2000)
                    log("g1 end")
                }
                launch {
                    delay(1000)
                    log("g2 end")
                }
                log("scope started")
            }
            log("scope end")
        }
    }

    "start without scope" {
        runBlocking {
            launch {
                delay(2000)
                log("g1 end")
            }
            launch {
                delay(1000)
                log("g2 end")
            }
            log("runBlocking started")
        }
        log("runBlocking end")
    }
})