package ch.barbulescu.coroutines

import io.kotest.common.runBlocking
import io.kotest.core.spec.style.StringSpec
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.channels.produce
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class ProducerConsumerTest : StringSpec({

    "producer and consumer with channels" {
        runBlocking {
            val channel = produceNumbers()
            launch {
                channel.consumeEach {
                    println("Consumed $it")
                }
            }
        }
    }

})

@OptIn(ExperimentalCoroutinesApi::class)
fun CoroutineScope.produceNumbers(): ReceiveChannel<Int> = produce {
    (1..5).forEach {
        send(it)
        println("Produced $it")
        delay(100)
    }
}

