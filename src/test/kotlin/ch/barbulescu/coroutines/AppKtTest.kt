package ch.barbulescu.coroutines

import io.kotest.core.spec.style.StringSpec
import io.kotest.property.Arb
import io.kotest.property.arbitrary.ArbitraryBuilder
import io.kotest.property.arbitrary.IntShrinker
import io.kotest.property.arbitrary.bind
import io.kotest.property.arbitrary.numbers.IntClassifier
import io.kotest.property.arbitrary.string
import io.kotest.property.checkAll
import kotlin.random.nextInt

class AppKtTest : StringSpec({
    "basic positive test" {
        checkAll(studentArb) { student ->
            println(student)
        }
    }
})

data class Student(val name: String, val age: Int)

val studentArb = Arb.bind(Arb.string(), Arb.age()) { name, age -> Student(name, age) }

fun Arb.Companion.age(): Arb<Int> {
    val range = 0..150
    return ArbitraryBuilder.create { it.random.nextInt(range) }
        .withShrinker(IntShrinker(range))
        .withClassifier(IntClassifier(range))
        .build()
}
