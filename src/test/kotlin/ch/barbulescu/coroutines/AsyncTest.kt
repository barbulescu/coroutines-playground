package ch.barbulescu.coroutines

import io.kotest.common.runBlocking
import io.kotest.core.spec.style.StringSpec
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay

class AsyncTest : StringSpec({

    "start in scope" {
        runBlocking {
            coroutineScope {
                val j1 = async(IO) {
                    delay(2000)
                    log("g1 end")
                    12
                }
                val j2 = async {
                    delay(1000)
                    log("g2 end")
                }
                log("scope started")
                log("j1 finished ${j1.await()}")
                j2.join()
            }
            log("scope end")
        }
    }

    "start without scope" {
        runBlocking {
            val j1 = async(IO) {
                delay(2000)
                log("g1 end")
            }
            val j2 = async {
                delay(1000)
                log("g2 end")
            }
            log("runBlocking started")
        }
        log("runBlocking end")
    }
})